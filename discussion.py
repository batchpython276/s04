class Sample_Class():

	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")

sample_obj = Sample_Class(2023)

print(sample_obj.year)
sample_obj.show_year()

# [Section] Fundamentals of OOP
# Four pillar of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction


# First Pillar: Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single

class Person():

	def __init__(self, name, age):
		self._name = name
		self._age = age


	# Methods
	# getter of _name attribute
	def get_name(self):
		print(f'Name of person: {self._name}')

	# Setter pf _name attributes
	def set_name(self, name):
		self._name = name

	# getter of _age attribute
	def get_age(self):
		print(f'Age of person: {self._age}')

	# setter of _age attribute
	def set_age(self, age):
		self._age = age



# new instance
persone_one = Person("James", 245)

print(persone_one._name)
persone_one.get_name()

persone_one.set_name("Chopooldo")
persone_one.get_name()

print(persone_one._age)
persone_one.get_age()

persone_one.set_age(17)
persone_one.get_age()


# Second Pillar: Inheritance
# the inheritance of the characteristic or attributes of a parent class to a child class that are derived from it

# Syntax: class child_class_name(parent_class_name):

class Employee(Person):
    def __init__(self, name, age, employee_id):
        super().__init__(name, age)
        self._employee_id = employee_id

    def get_employee_id(self):
        print(f"The employee ID is {self._employee_id}")

    def set_employee_id(self, employee_id):
        self._employee_id = employee_id

employee_one = Employee("CongTV", 27, "0001")
employee_one.get_age()

employee_one.set_age(13)
employee_one.get_age() 

employee_one.get_name()
employee_one.set_name("Barberos TV")
employee_one.get_name()


# Third Pillar: Polymorphism
# Functions and objects



class Team_Lead():
	def occupation(self):
		print('Team Lead')

	def has_auth(self):
		print(True)

class Team_Member():
	def occupation(self):
		print('Team Member')


	def has_auth(self):
		print(False)


team_lead = Team_Lead()
team_member = Team_Member()


for person in (team_lead, team_member):
	person.occupation()

# Polymorphism with inheritance
# Polymorphism in python defines methods in the child class that have the same name as the methods in the parent class

class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks(developer career, pi-shape career and short courses)')
	def num_of_hours(self):
		print('Learn web development in 360 hours!')


class Developer_Career(Zuitt):
	# overriding of the parent method
	def num_of_hours(self):
		print('Learn the basics of web dev in 240 hours!')

class Pi_Shape_Career(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code app dev in 140 hours!")

class Short_Course(Zuitt):
	def num_of_hours(self):
		print("Learn advance topics in web dev in 20 hours!")

course_one = Developer_Career()
course_two = Pi_Shape_Career()
course_three = Short_Course()

for course in (course_one, course_two, course_three):
	course.tracks()
	course.num_of_hours()


# Fourth Pillar: Abstraction
# an abstract class can be considered as a blueprint for other classes. it allows you to create a set of methods that must be created within any child classes built from abstract class

# ABC - abstarct base classes
from abc import ABC
# this import tells the program to get the abc module of pythin to be used


class Polygon(ABC):
	def print_number_sides(self):
		# The pass keyword it denotes that the method doesnt do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f"This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f"This polygon has 5 sides")


shape_one = Triangle()
shape_two = Pentagon()

shape_one.print_number_of_sides()
shape_two.print_number_of_sides()















