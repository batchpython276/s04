from abc import ABC, abstractmethod


class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def get_name(self):
        return self._name
    
    def set_name(self, name):
        self._name = name
    
    def get_breed(self):
        return self._breed
    
    def set_breed(self, breed):
        self._breed = breed
    
    def get_age(self):
        return self._age
    
    def set_age(self, age):
        self._age = age
    
    def eat(self, food):
        print(f"Serve me {food}")
    
    def make_sound(self):
        print("Meow! Nyaw! Nyaaaaa!")
    
    def call(self):
        print(f"Puss, come on!")

class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def get_name(self):
        return self._name
    
    def set_name(self, name):
        self._name = name
    
    def get_breed(self):
        return self._breed
    
    def set_breed(self, breed):
        self._breed = breed
    
    def get_age(self):
        return self._age
    
    def set_age(self, age):
        self._age = age
    
    def eat(self, food):
        print(f"Eaten {food}")
    
    def make_sound(self):
        print("Bark! Woof! Arf!")
    
    def call(self):
        print(f"Here {self._name}!")

cat = Cat("Puss", "Siamese", 3)
dog = Dog("Isis", "Labrador", 5)

dog.eat("Steak")
dog.make_sound()
dog.call()

cat.call()
cat.eat("Tuna")
cat.make_sound()
